var storage = {};

export const StoreKey = {
    Log: "Log",
    Account: "account",
    SoundSwitch: "SoundSwitch",
    MusicSwitch: "MusicSwitch",
    cookie: "cookie"
};

function getID(): string {
    return "UserData";//Login.playerId + "";
}

function load() {
    let str = cc.sys.localStorage.getItem(getID()) || "{}";
    let data = JSON.parse(str);
    if (data.constructor != Object)
        data = {};
    // console.log("Storage.load " + getID() + "\n" + str);
    storage[getID()] = data;
    return data;
}

export function getItem(key) {
    let data = storage[getID()] || load();
    let value = data[key];
    //console.log("Storage.get " + key + " is " + value);
    return value;
}

let noMark;
export function setItem(key, value) {
    if (noMark)
        return;

    let data = storage[getID()] || load();
    data[key] = value;
    let str = JSON.stringify(data);
    try {
        localStorage.setItem(getID(), str);
        cc.sys.localStorage.setItem(getID(), str);
    }
    catch (e) {
        if (!noMark)
            alert("您的浏览器已启用无痕模式！");
        noMark = true;
    }
    // console.log("Storage.set " + getID() + "\n" + key + "\n" + str);
}

export function setSoundSwitch(value: boolean) {
    cc.sys.localStorage.setItem(StoreKey.SoundSwitch, value ? "true" : "false");

}

export function getSoundSwitch() {
    let data = storage[StoreKey.SoundSwitch] || cc.sys.localStorage.getItem(StoreKey.SoundSwitch) || "true";
    storage[StoreKey.SoundSwitch] = data;
    return data == "true";
}

export function setMusicSwitch(value: boolean) {
    cc.sys.localStorage.setItem(StoreKey.MusicSwitch, value ? "true" : "false");
}

export function getMusicSwitch() {
    let data = storage[StoreKey.MusicSwitch] || cc.sys.localStorage.getItem(StoreKey.MusicSwitch) || "true";
    storage[StoreKey.MusicSwitch] = data;
    return data == "true";
}