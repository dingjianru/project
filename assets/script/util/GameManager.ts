export class GameManager{
    private static _instance: GameManager;
	public static getInstance(): GameManager {
		if (!GameManager._instance) {
            GameManager._instance = new GameManager();
        }
		return GameManager._instance;
    }
}