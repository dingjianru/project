import { ViewManager } from "./ViewManager";
import { RES } from "./RES";
import { getItem, setItem } from "./LocalStorage";
// import FloatTextNode from "../view/FloatTextNode";
/**
 * 格式化字符串，使用params中的元素替换字符串中{n}表示的占位符
 * @param template 
 * @param params 
 */
export function stringFormat(template: string, params: any[]): string {
    return template.replace(/\{([0-9]+?)\}/g, (match, index) => params[index] || `\{${index}\}`);
}

export function onNumberEditDidEnded(editbox, customEventData) {
    let _value = Math.floor(Number(editbox.string));
    editbox.string = _value;
}
/**
 * 
 * */
export function checkEmail(str) {
    let myReg = /^[0-9a-zA-Z_.-]+[@][0-9a-zA-Z_.-]+([.][a-zA-Z]+){1,2}$/;
    if(myReg.test(str)){
        return true;
    }else{
        return false;
    }
}

/**
 *
 * */
export function checkAccount(str) {
    let myReg = /^[0-9a-zA-Z]+[@][0-9a-zA-Z]{2,10}$/;
    if(myReg.test(str)){
        return true;
    }else{
        return false;
    }
}


export function checkPhone(str) {
    let myReg = /^\d{10}$/;
    if(myReg.test(str)){
        return true;
    }else{
        return false;
    }
}

export function checkPlayerID(str) {
    let myReg = /^\d{8}$/;
    if(myReg.test(str)){
        return true;
    }else{
        return false;
    }
}

export function checkOTP(str:string) {
    let myReg = /^\d{6}$/;
    if(myReg.test(str)){
        return true;
    }else{
        return false;
    }
}
//获取是否包含数字
export function containsNumber(str) {
    var reg=/\d/;
    return reg.test(str);
}
//获取字符串长度
export function getStrlen(str) {
    var len = 0;
    for (var i=0; i<str.length; i++) { 
        var c = str.charCodeAt(i); 
        //单字节加1 
        if ((c >= 0x0001 && c <= 0x007e) || (0xff60<=c && c<=0xff9f)) { 
            len++; 
        }else{ 
            len+=2; 
        } 
    } 
    return len;
}

export function checkName(str) {
    var pattern = new RegExp("[`~!@#$^&*()=|{}':;',\\[\\].<>/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？]");
    if(pattern.test(str)){
        return false;
    }
    if(str == "" || containsNumber(str) || getStrlen(str) > 50){
        return false;
    }
    return true;
}
/**
 * 根据材料、装本品质    改变富文本字体颜色
 * @param quality 品质
 * @param str 文本
 */
export function changeRichTextColor(quality: number, inforStr: string, str?: string): string {
    var tempStr: string = "";
    var color: string = "";
    if (quality >= 0 && quality < 3) {
        color = "<color=#008000>";
    }
    else if (quality >= 3 && quality < 6) {
        color = "<color=#3366FF>";
    }
    else if (quality >= 6 && quality < 9) {
        color = "<color=#800080>";
    }
    else {
        color = "<color=#FF6600>";
    }
    tempStr = color + inforStr + "</color>";
    return tempStr;
}

/**
 * 根据品质改变文本颜色
 * @param label 文本
 * @param quality 品质
 */
export function changeLabelColor(label: cc.Label, inforStr: string, quality: number) {
    label.string = inforStr;
    var color = cc.Color.BLACK;
    if (quality >= 0 && quality < 3) {
        color.fromHEX("#008000");
    }
    else if (quality >= 3 && quality < 6) {
        color.fromHEX("#3366FF");
    }
    else if (quality >= 6 && quality < 9) {
        color.fromHEX("#800080");
    }
    else {
        color.fromHEX("#FF6600");
    }
    label.node.color = color;
}

/**
 * 获取战斗时的战斗icon
 * @param base 表中配置的基础名
 */
export function getAttackIcon(base: string) {
    return base + "_2";
}

type TransformInfo = { pos: cc.Vec2, rotation: number, scale: cc.Vec2, color: cc.Color, opacity: number, skew: cc.Vec2 };
/**
 * 返回node的Transform信息
 * @param node 
 */
export function copyTransformInfo(node: cc.Node): TransformInfo {
    let pos = new cc.Vec2(node.position.x, node.position.y);
    let rotation = node.rotation;
    let scale = new cc.Vec2(node.scaleX, node.scaleY);
    let color = node.color.clone();
    let opacity = node.opacity;
    let skew = new cc.Vec2(node.skewX, node.skewY);
    return { pos, rotation, scale, color, opacity, skew };
}

/**
 * 使用Transform信息设置node
 * @param node 
 */
export function revertTransformInfo(node: cc.Node, transformInfo: TransformInfo) {
    node.setPosition(transformInfo.pos);
    node.rotation = transformInfo.rotation;
    node.setScale(transformInfo.scale);
    node.color.set(transformInfo.color);
    node.opacity = transformInfo.opacity;
    node.skewX = transformInfo.skew.x;
    node.skewY = transformInfo.skew.y;
}

/**
 *  转换时间格式
 * */
export function secondsToTime(t: number) {

    if (t < 0) {
        return "00:00:00";
        // return "00:00";
    }
    let h = Math.floor(t / 3600);
    t = (t - (h * 3600));
    let m = Math.floor(t / 60);
    t = (t - (m * 60));
    let s = Math.floor(t);


    let _h = (h >= 10 ? h : "0" + h) + ":";
    let _m = (m >= 10 ? m : "0" + m) + ":";
    let _s = s >= 10 ? s : "0" + s;

    return _h + _m + _s;
}

/**
 *  转换时间格式
 * */
export function secondsToTimeNew(t: number) {
    if (t < 0) {
        return "00:00:00";
    }
    let d = Math.floor(t / 3600 / 24);
    t = (t - (d * 3600 * 24));
    let h = Math.floor(t / 3600);
    t = (t - (h * 3600));
    let m = Math.floor(t / 60);
    t = (t - (m * 60));
    let s = Math.floor(t);

    let _d = "";
    let _h = (h >= 10 ? h : "0" + h) + ":";
    let _m = (m >= 10 ? m : "0" + m) + ":";
    let _s = s >= 10 ? s : "0" + s;
    if(d > 0){
        _d = d + " Day";
        _h = "";
        _m = "";
        _s = "";
    }
    else{
        _d = "";
    }
    return _d + _h + _m + _s;
}

export function showErrerMsg(_errorCode){
    if(_errorCode != 0){
        let _key = "KEY_ERROR_" + _errorCode;
        showTip(_key);
    }
}

/**
 * 飘字
 * @param msg 
 */
export async function showTip(msg: string) {
    // let res = await RES.loadRes("prefab/FloatTextNode", cc.Prefab);
    // if (res){
    //     let top = ViewManager.getTopLayer();
    //     let tip: cc.Node = cc.instantiate(res);
    //     tip.parent = top;
    //     tip.zIndex = 99;
    //     tip.getComponent(FloatTextNode).init(msg);
    // }
}
/**
 * Async tasks
 * */
export function series(tasks, completeCallback) {
    let done = function () {
        let task = tasks.shift();
        if (task) task(done);
        else completeCallback();
    };
    done();
}

/**
 * Clone obj
 */
export function clone(obj) {
    let o = obj.constructor ? new obj.constructor() : {};
    for (let prop in obj) {
        if (!obj.hasOwnProperty(prop)) continue;
        let value = obj[prop];
        if (typeof value == "object" && value != null) {
            o[prop] = clone(value);
        } else {
            o[prop] = value;
        }
    }
    return o;
}

/**
 * 升级成功
 * @param msg 
 */
export async function upgradeSuccess() {
    // let res = await RES.loadRes("prefab/item/UpgradeEffect", cc.Prefab);
    // if (!res)
    //     return;
    // let top = ViewManager.getTopLayer();
    // let effect: cc.Node = cc.instantiate(res);
    // effect.parent = top;
}

//倒计时
/**
 * 
 * @param fmt hh:mm:ss
 * @param time 剩余秒数
 */
export function timeReduce(fmt: string, time: number) {
    let dayUnit = 24 * 60 * 60 * 1;
    let day = (Math.floor(time / dayUnit) + 100 + "").substring(1);
    if (day == "00")
        fmt = fmt.replace(/dd.*hh/, "hh");
    time = time % dayUnit;
    let hoursUnit = 60 * 60 * 1;
    let hours = (Math.floor(time / hoursUnit) + 100 + "").substring(1);
    if (hours == "00")
        fmt = fmt.replace(/hh.*mm/, "mm");
    time = time % hoursUnit;
    let minutesUnit = 60 * 1;
    let minutes = (Math.floor(time / minutesUnit) + 100 + "").substring(1);
    if (fmt.indexOf(":") == -1 && minutes == "00")
        fmt = fmt.replace(/mm.*ss/, "ss");
    time = time % minutesUnit;
    let secondsUnit = 1;
    let seconds = (Math.floor(time / secondsUnit) + 100 + "").substring(1);
    if (fmt.indexOf(":") == -1 && seconds == "00")
        fmt = fmt.replace(/ss.*/, "");
    return fmt
        .replace("dd", day)
        .replace("hh", hours)
        .replace("mm", minutes)
        .replace("ss", seconds)
}

export function toLocaleDateString(value){
    // console.log("new Date(parseInt(value) * 1000).getTime().toLocaleDateString()==" + new Date(value * 1000).toTimeString());
    // console.log("new Date(parseInt(value) * 1000).getTime().toLocaleDateString()==" + new Date(value * 1000).toDateString());
    // console.log("new Date(parseInt(value) * 1000).getTime().toLocaleDateString()==" + new Date(value * 1000).toJSON());
    return  new Date(value * 1000).toLocaleDateString();
}

/**
 * 获取多语言字段内容
 * @param id 
 */
export function getI18nString(id, params?:number[]|string[]): string {
    // return LanManager.getlocal(id, params);
    return "";
}


/**
 * 检查相应货币类型资源是否足够
 * @param key 
 * @param count 
 * @param isShowTip 
 * @param tipKey 
 */
// export function checkCurrency(key: com.boss.proto.CommonPropDeclare, count: number, resName: number = 0, tipKey: number = 5008): boolean {
//     if (UserBO.self.getCommonProp(key) < count) {
//         resName && showTip(stringFormat(getI18nString(tipKey), [getI18nString(resName)]));
//         return false;
//     }
//     return true;
// }

/**
 *  服务器统一的错误提示
 *  @param errorCode
 * */
export function serverErrorTip(errorCode: number) {
    // let error = def.loadServerError(errorCode + "");
    // if (error) {
    //     showTip(getI18nString(error.errorId));
    // } else {
    //     console.warn("No error code found:", errorCode);
    // }
}

/*
     * 随机数
     * */
export function random(min, max) {
    if (max == null) {
        max = min;
        min = 0;
    }

    let n = min + (Math.random() * (max - min + 1));

    return Math.floor(n);
}

/**
 * 替换字符
 * */
export function charsReplace(str, a, b) {
    let _str = "";
    for (let index = 0; index < str.length; index++) {
        let c = str[index];
        if (c == a) {
            _str += b;
        } else {
            _str += c;
        }
    }
    return _str;
}


/**
 * HTML转义
 * */
export function HTMLEncode(html) {
    var temp = document.createElement("div");
    (temp.textContent != null) ? (temp.textContent = html) : (temp.innerText = html);
    var output = temp.innerHTML;
    temp = null;
    return output;
}
/**
 *HTML反转义
 * */
export function HTMLDecode(text) {
    var temp = document.createElement("div");
    temp.innerHTML = text;
    var output = temp.innerText || temp.textContent;
    temp = null;
    return output;
}

export function preloadScene(sceneName){
    cc.director.preloadScene(sceneName, function (count, maxcount, item) {
        let cmd = {
            count: count,
            maxCount: maxcount
        };
    }, function () {
        cc.director.loadScene(sceneName, function () {
            cc.log("load "+sceneName+" finish...");
        });
    });
}
// //给定版本号大于等于包版本号返回true   小于返回false   包含当前版本
// export function checkGreaterVersionCode(version){
//     let _curVersion = CallNative.getInatance().getPackageVersion();
//     if(version == _curVersion)return true;
//     let _checkPackageVersion = version.split("."); 
//     let _curPackageVersion = _curVersion.split(".");
    
//     for(let i = 0 ; i < _curPackageVersion.length; i ++){
//         if(_curPackageVersion[i] != undefined && _checkPackageVersion[i] != undefined){
//             if(_curPackageVersion[i] > _checkPackageVersion[i]){
//                 return true;
//             }else if(_curPackageVersion[i] < _checkPackageVersion[i]){
//                 return false;
//             };
//         }
//     }
//     return false;
// }

export function checkUpdate(version){
    // let toNum = function (v){
    //     let a=v + "";
    //     let c=a.split('.');
    //     let num_place=["","0","00","000","0000"],r=num_place.reverse();
    //     for (let i=0;i<c.length;i++){
    //         let len=c[i].length;
    //         c[i]=r[len]+c[i];
    //     }
    //     let res= c.join('');
    //     return res;
    // };
    // let _a=toNum(Config.gameVersion),_b=toNum(version);
    // if(_a <= _b){
    //     return true;
    // }
    return false;
}

//获取筹码
export function getChips(_chipArray:Array<Object>, _value:number){
    let _result = []

    let sortedArray: Array<Object> = _chipArray.sort((n1,n2) => {
        if (n1["value"] > n2["value"]) return -1;
        if (n1["value"] < n2["value"]) return 1;
        return 0;
    });

    for(let i = 0;i < sortedArray.length; i ++){
        let _stepValue = sortedArray[i]["value"];
        let _count = Math.floor(_value / _stepValue);
        for(let j = 0; j < _count; j++){
            _result.push(sortedArray[i]);
        }
        _value = _value % _stepValue;
        if(_value <= 0) break;
    }
    return _result;
}

export async function loadPokerRes(_card, _poker){
    let _url = "poker/poker_color_"+_card.Color+"_num_"+_card.Num;
    let res = await RES.loadRes(_url, cc.SpriteFrame);
    if (res){
        _poker.spriteFrame = res;
    }else{
        cc.log("load res fail url :" + _url);
    }
}
// export function getPokerNumURL(_card){
//     let _numValue = _card.Num == 14 ? 1 : _card.Num;
//     if(_card.Color == 5){
//         return pokerLabelString[pokerLabelString.length - 1];
//     }else{
//         return pokerLabelString[_numValue - 1];
//     }
// }
// export function getPokerNumColorURL(_card){
//     if(_card.Color == 5){
//         return (_card.Num == 1 ? pokerColorDark : pokerColorRed);
//     }else if(_card.Color % 2 == 0){
//         return pokerColorRed;
//     }
//     return pokerColorDark;
// }

export function loadPokerColorURL(_card){
    let _numValue = _card.Num == 14 ? 1 : _card.Num;
    if(_card.Color == 5){
        return (_numValue == 1 ? "joker_02" : "joker_01");
    }else {
        return ("color_" + _card.Color);
    }
}
export function loadPokerBigColorURL(_card){
    let _numValue = _card.Num == 14 ? 1 : _card.Num;
    if(_card.Color == 5){
        return (_numValue == 1 ? "joker_02" : "joker_01");
    }else if(_numValue <= 10){
        return ("color_" + _card.Color);
    }else{
        return ("color_" + _numValue);
    }
}

// export async function loadPokerAtlas(_card, _num, _color1, _color2){
//     let _numValue = _card.Num == 14 ? 1 : _card.Num;
//     let _url = "poker/poker";
//     let atlas = await RES.loadRes(_url, cc.SpriteAtlas);
//     if (atlas){
//         if(_card.Color == 5){
//             _num.spriteFrame = atlas.getSpriteFrame(_numValue == 1 ? "joker_02_01" : "joker_01_01");
//             _color1.spriteFrame = atlas.getSpriteFrame(_numValue == 1 ? "joker_02" : "joker_01");
//             _color2.spriteFrame = atlas.getSpriteFrame(_numValue == 1 ? "joker_02" : "joker_01");
//         }else{
//             _num.spriteFrame = atlas.getSpriteFrame((_card.Color % 2 == 0 ? "r" : "b") + _numValue);
//             if(_numValue <= 10){
//                 _color1.spriteFrame = atlas.getSpriteFrame("color_" + _card.Color);
//                 _color2.spriteFrame = atlas.getSpriteFrame("color_" + _card.Color);
//             }else if(_numValue > 10){
//                 _color2.spriteFrame = atlas.getSpriteFrame("color_" + _card.Color);
//                 _color1.spriteFrame = atlas.getSpriteFrame("color_" + _card.Color + "_" + _numValue);
//             }
//         }
//     }else{
//         cc.log("load res Atlas fail url :" + _url);
//     }
// }

export async function loadPokerBG(_small = false){
    let _url = "poker/bg_back";
    if(_small) _url = "poker/bg_back_small";
    let res = await RES.loadRes(_url, cc.SpriteFrame);
    if (res){
        var node = new cc.Node();
        const sprite = node.addComponent(cc.Sprite);
        sprite.spriteFrame = res;
        node.parent = this.node;
        return node;
    }
    return null;
}

/**获取系统时间 */
// export async function getServerTime(){
//     Config.curServerTime = new Date().getTime();
//     if(cc.sys.isMobile){
//         let _url = "http://api.m.taobao.com/rest/api3.do?api=mtop.common.getTimestamp";
//         let _serverJSON = await this.sendHttpGetRequest(_url);
//         if(_serverJSON){
//             Config.curServerTime = Number(_serverJSON["data"]["t"]);
//         }
//     }
//     return  Config.curServerTime;
//  }

export function sendHttpGetRequest(url, paramsArr = []): Promise<JSON>{
    let dataStr = ''; 
    for(let i = 0; i < paramsArr.length; i++){
        let params = paramsArr[i];
        Object.keys(params).forEach(key => {
            dataStr += key + '=' + encodeURIComponent(params[key]) + '&';
        })
    }
    if (dataStr !== '') {
        dataStr = dataStr.substr(0, dataStr.lastIndexOf('&'));
        url = url + '?' + dataStr;
    }
    return new Promise(res => {
        let _receiveHTTP = setTimeout(() => {
            res(null);
        }, 5500);
        let xhr = cc.loader.getXMLHttpRequest();
        xhr.open("GET", url, true);
        xhr.setRequestHeader("Content-Type","text/plain;charset=UTF-8");  
        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4) {
                let response = xhr.responseText;
                if (xhr.status >= 200 && xhr.status <= 400) {
                    res(JSON.parse(response));
                }else{
                    res(null);
                }
                clearTimeout(_receiveHTTP);
            }
        };
        xhr.ontimeout = function(event){
        　　res(null);
        }
        xhr.timeout = 5000;
        xhr.send();
    })
}

export function formatStr(str, ...args:any[]){
    var l = args.length;
    if(l < 1)
        return str;

    var needToFormat = true;
    if(typeof str === "object"){
        needToFormat = false;
    }
    for(var i = 0; i < l; ++i){
        var arg = args[i];
        if(needToFormat){
            while(true){
                var result = null;
                if(typeof arg === "number"){
                    result = str.match(/(%d)|(%s)/);
                    if(result){
                        str = str.replace(/(%d)|(%s)/, arg);
                        break;
                    }
                }
                result = str.match(/%s/);
                if(result)
                    str = str.replace(/%s/, arg);
                else
                    str += " " + arg;
                break;
            }
        }else
            str += " " + arg;
    }
    return str;
};

export function getRandom(){
    let _arr = [];
    for(let i = 0 ; i < 20 ; i ++){
        _arr.push(Math.random());
    }
    return _arr[Math.floor(Math.random() * _arr.length)]
};

export function getClientMoney(_value:number, _fixKey = 2){
    if(_fixKey > 4) _fixKey = 4;
    let _resultStr = "";
    let _str = _value + "";
    for(let i = 0;i < 4; i++){
        if(_str.length <= i){
            _str = "0" + _str;
        }
    }
    
    let length = _str.length >= 4 ? _str.length : 4;
    let i = length - 1 - (4 - _fixKey);
    while( i >= 0){
        _resultStr = _str[i] + _resultStr;
        if(i == length - 4)_resultStr ='.' + _resultStr ;
        i--;
    }
    return Number(_resultStr) + "";
}

export function getMoneyFormat(_value:number, _fixValue = 2){
    // _fixValue = Math.pow(10, _fixValue);
    // if(_value < 100000){ //10W = 1L
    //     return getMoneyDetail(_value);
    // }else if(_value <= 1000000){
    //     return Math.floor(_value / 100000 * _fixValue) / _fixValue + "L";
    // }else if(_value <= 10000000){
    //     return Math.floor(_value / 1000000 * _fixValue) / _fixValue + "M";
    // }else if(_value <= 100000000){
    //     return Math.floor(_value / 10000000 * _fixValue) / _fixValue + "Cr";
    // }else{
    //     return Math.floor(_value / 100000000 * _fixValue) / _fixValue + "B";
    // }
    return getClientMoney(_value, _fixValue);
}

export function getMoneyDetail(_value:number, _fixValue = 2){
    // let _result = "";
    // if(_value !== null){
    //     let _numArr = (""+_value);//.split("");
    //     let _index = 0;
    //     for(let i = 0; i < _numArr.length ; i++){
    //         _result = _result + _numArr[i];
    //         _index = _numArr.length - i -1
    //         if((_index > 3 && _index % 2 == 1) || _index == 3){
    //             _result = _result + ",";
    //         }
    //     }
    // }
    // return _result;
    return getClientMoney(_value, _fixValue);
}

export function getMoneyBigValue(_value:number){
    // if(_value >= 100000000){ //10W = 1L
    //    return getMoneyFormat(_value);
    // }
    // return getMoneyDetail(_value);
    return getClientMoney(_value);
}

//转换.99999数字为1
export function signFigures(num, rank = 6){
    if(!num) return(0);
    const sign = num / Math.abs(num);
    const number = num * sign;
    const temp = rank - 1 - Math.floor(Math.log10(number));
    let ans;
    if (temp > 0) {
        ans = parseFloat(number.toFixed(temp));
    } else if (temp < 0) {
        const temps = Math.pow(10, temp);
        ans = Math.round(number / temps) * temps;
    } else {
        ans = Math.round(number);
    }
    return (ans * sign);
}


////////////////////////////////////////////
export function formatNikeName(pStr, pLen = 8) {
    var _ret = cutString(pStr, pLen);
    var _cutFlag = _ret.cutflag;
    var _cutStringn = _ret.cutstring;

    if ("1" == _cutFlag) {
        return _cutStringn + "...";
    } else {
        return _cutStringn;
    }
}
export function cutString(pStr, pLen) {
    var _strLen = pStr.length;
    var _tmpCode;
    var _cutString;
    var _cutFlag = "1";
    var _lenCount = 0;
    var _ret = false;

    if (_strLen <= pLen/2) {
        _cutString = pStr;
        _ret = true;
    }
    if (!_ret) {
        for (var i = 0; i < _strLen ; i++ ) {
            if (isFull(pStr.charAt(i))) {
                _lenCount += 2;
            } else {
                _lenCount += 1;
            }

            if (_lenCount > pLen) {
                _cutString = pStr.substring(0, i);
                _ret = true;
                break;
            } else if (_lenCount == pLen) {
                _cutString = pStr.substring(0, i + 1);
                _ret = true;
                break;
            }
        }
    }
    if (!_ret) {
        _cutString = pStr;
        _ret = true;
    }
    if (_cutString.length == _strLen) {
        _cutFlag = "0";
    }
    return {"cutstring":_cutString, "cutflag":_cutFlag};
}

export function isFull (pChar) {
    for (var i = 0; i < pChar.strLen ; i++ ) {    
        if ((pChar.charCodeAt(i) > 128)) {
            return true;
        } else {
            return false;
        }
    }
}

export function concatTable(tab1, tab2){
    for(let i = 0; i < tab2.length; i++){
        tab1.push(tab2[i]);
    }
    return tab1;
}

export function arrayBufferToBase64(bytes) {
    var base64 = '';
    var encodings = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/';
    var byteLength = bytes.byteLength;
    var byteRemainder = byteLength % 3;
    var mainLength = byteLength - byteRemainder;
    var a, b, c, d;
    var chunk;
    // Main loop deals with bytes in chunks of 3
    for (var i = 0; i < mainLength; i = i + 3) {
        // Combine the three bytes into a single integer
        chunk = (bytes[i] << 16) | (bytes[i + 1] << 8) | bytes[i + 2];
        // Use bitmasks to extract 6-bit segments from the triplet
        a = (chunk & 16515072) >> 18; // 16515072 = (2^6 - 1) << 18
        b = (chunk & 258048) >> 12; // 258048 = (2^6 - 1) << 12
        c = (chunk & 4032) >> 6; // 4032 = (2^6 - 1) << 6
        d = chunk & 63; // 63 = 2^6 - 1
        // Convert the raw binary segments to the appropriate ASCII encoding
        base64 += encodings[a] + encodings[b] + encodings[c] + encodings[d];
    }
    // Deal with the remaining bytes and padding
    if (byteRemainder == 1) {
        chunk = bytes[mainLength];
        a = (chunk & 252) >> 2 // 252 = (2^6 - 1) << 2;
        // Set the 4 least significant bits to zero
        b = (chunk & 3) << 4 // 3 = 2^2 - 1;
        base64 += encodings[a] + encodings[b] + '==';
    }
    else if (byteRemainder == 2) {
        chunk = (bytes[mainLength] << 8) | bytes[mainLength + 1];
        a = (chunk & 16128) >> 8 // 16128 = (2^6 - 1) << 8;
        b = (chunk & 1008) >> 4 // 1008 = (2^6 - 1) << 4;
        // Set the 2 least significant bits to zero
        c = (chunk & 15) << 2 // 15 = 2^4 - 1;
        base64 += encodings[a] + encodings[b] + encodings[c] + '=';
    }
    return base64;
}

// //shareType   0分享链接   1分享图片
// export function shareFacebook(_info) {
//     let _shareStr = "{";
//     let _key = _info.shareType == undefined ? "" : _info.shareType;
//     _shareStr = _shareStr + "shareType:" + _key + ",";

//     _key = _info.shareUrl == undefined ? "" : _info.shareUrl;
//     _shareStr = _shareStr + "shareUrl:'" + _key + "',";

//     _key = _info.imgPath == undefined ? "" : _info.imgPath;
//     _shareStr = _shareStr + "imgPath:'" + _key + "',";

//     _key = _info.shareText == undefined ? "" : _info.shareText;
//     _shareStr = _shareStr + "shareText:'" + _key + "'}";

//     CallNative.getInatance().facebookShare(_shareStr);
// }

// export function ShopBuyItem(_info) {
//     let _keyValue = ["type", "productId", "orderId", "hash", "amount", "phone", "name", "email", "furl", "surl", "key", "id", "userID", "debug"];
//     let _buyStr = "{";
//     for(let i = 0 ; i < _keyValue.length; i++){
//         let _key = _info[_keyValue[i]] == undefined ? "" : _info[_keyValue[i]];
//         _buyStr = _buyStr + _keyValue[i] + ":'" + _key;
//         _buyStr = _buyStr + ((i == _keyValue.length - 1) ? "'}" : "',");
//     }
//     cc.log("----------------shopBuyItem " + _buyStr);
//     CallNative.getInatance().shopBuyItem(_buyStr);
// }

// export function dot(_key, _every = false){
//     cc.log(">>>>>>>>>>>>>>>>>>>>>>>dot:" + _key)
//     if(cc.sys.isMobile){
//         let dot = getItem("dotkey_" + _key) || "";
//         if(dot == "" || _every){
//             setItem("dotkey_" + _key, "1");
//             let channelId = CallNative.getInatance().getChannelID();
//             let apkId = CallNative.getInatance().getApkID();
//             let packageId = CallNative.getInatance().getPackageVersion();
            
//             let _time = Math.floor((new Date().getTime()) / 1000);
//             let base_str = "c843ed2b09a46ffe94ed2a614fd8df13";
//             let sign = (new StringMD5()).hex_md5("/mark?type=" + _key + "&channelId=" + channelId + "&apkId=" + apkId + "&packageId=" + packageId + "&time="+_time+base_str).toLowerCase();
//             let _url = (Config.DEBUG_MODE ? ConfigTest.HTTP_GET_DOT_URL : Config.HTTP_GET_DOT_URL) + "mark";
//             sendHttpGetRequest(_url, [{type:_key}, {channelId:channelId}, {apkId:apkId}, {packageId:packageId}, {time:_time}, {sign:sign}]);
//         }
//     }
// }

// export function dotAFEvent(_key, _once = true, _value = "{}"){
//     cc.log(">>>>>>>>>>>>>>>>>>>>>>>dotAF:" + _key)
//     if(cc.sys.isMobile){
//         let dot = getItem("AFdotkey_" + _key) || "";
//         if(dot == ""){
//         // if(dot == "" || _once == false){
//             setItem("AFdotkey_" + _key, "1");
//             CallNative.getInatance().trackEvent(_key, _value);
//         }
//     }
// }

export function seekNodeByName(_node, _name){
    if (_node == null){
        return null
    }
    
    if (_node.name == _name){
        return _node
    }
    
    let arryRootChild = _node.getChildren();
    let length = arryRootChild.length;
    for(let i = 0; i < length; i++){
        let child = arryRootChild[i];
        if (child != null){
            let res = seekNodeByName(child, _name)
            if(res != null){
                return res
            }
        }
            
    }
    return null;
}
// //检测黑名单渠道  （所有上gp渠道的包都需要检测）
// export function checkChannelBlack(){
//     let _channel = CallNative.getInatance().getApkID();
//     for(let i = 0; i < examineCheckApkChanel.length; i++){
//         if(_channel == examineCheckApkChanel[i]){
//             return 1;
//         }
//     }
//     return 0;
// }

// export function checkShowType(_inBlack, _gameCount){
//     PlayerBO.getInstance().GameCount = _gameCount;
//     GameManager.getInstance().game_examine_back = _inBlack;
// }

// //是否限制包状态
// export function limitGame(){
//     //测试包 且跳过限制机制
//     if(Config.DEBUG_MODE && ConfigTest.JumpBlack){
//         return false;
//     }
//     //黑名单用户 优先级最高 限制
//     if(GameManager.getInstance().game_examine_back == 1){
//         return true
//     }
//     //服务器记录为自然用户
//     if(PlayerBO.getInstance().AfStatus == 1){
//         return false;
//     }
//     //非自然用户  不限制
//     if(PlayerBO.getInstance().NonOrganic){
//         return false;
//     }
//     //特殊渠道不限制
//     if(checkChannelBlack() == 0){
//         return  false;
//     }
//     //游客  限制
//     if(PlayerBO.getInstance().IsTourist){
//         return true;
//     }
//     //把数够了  不限制
//     if(PlayerBO.getInstance().GameCount >= GameManager.getInstance().GameExamineRounds()){
//         return false;
//     }
//     return true;
// }

// /////////////////////////////////////////////////////////
// export function getKYCEnable(){
//     let _apkID = CallNative.getInatance().getApkID();
//     for(let i = 0; i < KYC_platform.length; i++){
//         if(_apkID == KYC_platform[i]){
//             return true;
//         }
//     }
//     return false;
// }

// export function getGPSEnable(){
//     let _apkID = CallNative.getInatance().getApkID();
//     for(let i = 0; i < GPS_platform.length; i++){
//         if(_apkID == GPS_platform[i]){
//             return true;
//         }
//     }
//     return false;
// }

export function getBigMoneyForServer(_value:number, _fixValue = 10000){
    return _value * _fixValue;
}

export function playDragonBonesAnim(_node:cc.Node, _anim){
    let _dragonBones: dragonBones.ArmatureDisplay = _node.getComponentInChildren(dragonBones.ArmatureDisplay);
    if(_dragonBones){
        _dragonBones.playAnimation(_anim, -1);
    }
}

// //_dotType 0 首充
// export function sendData2Backgroud(_userID, _dotType, _info){
//     if(cc.sys.isMobile){
//         let apkId = CallNative.getInatance().getApkID();
//         let packageId = CallNative.getInatance().getPackageVersion();

//         let _url = "http://online.xyzg.top:3003/dotdata";
//         if(Config.DEBUG_MODE){
//             _url = "http://172.31.11.249:3002/dotdata";
//         }
//         let base_str = "15fead47be56b308";
//         let _md5Str = _userID+""+_dotType+JSON.stringify(_info)+apkId+packageId+base_str
//         let _md5 = (new StringMD5()).hex_md5(_md5Str).toLowerCase();

//         let _params = { UserId:_userID, DotType:_dotType, ApkId:apkId, PackageId:packageId, Md5:_md5, Info: _info}
//         let _receiveHTTP = setTimeout(() => {
            
//         }, 6500);

//         let xhr = cc.loader.getXMLHttpRequest();
//         xhr.onreadystatechange = function () {
//             if (xhr.readyState === 4) {
//                 if (xhr.status >= 200 && xhr.status < 300) {
                    
//                 } else {
                    
//                 }
//                 clearTimeout(_receiveHTTP);
//             }
//         };
//         xhr.ontimeout = function(event){
//         　　
//         }
//         xhr.open('POST', _url, true);
//         xhr.setRequestHeader("Content-Type", "application/json");
//         xhr.timeout = 6000;
//         xhr.send(JSON.stringify(_params));
//         // cc.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>_md5Str:" + _md5Str);
//         // cc.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>_url:" + _url);
//         // cc.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>_params:" + JSON.stringify(_params));
//     }
// }

// export function dotFirstPurge(_isFirst, _index){
//     let _pre = getItem("shopFirst") || "";
//     if(_pre == ""){
//         if(_isFirst){
//             setItem("shopFirst", "0");
//         }else{
//             setItem("shopFirst", "1");
//         }
//     }else{
//         if(_pre == "0" && _isFirst == false){
//             setItem("shopFirst", "1");
//             let _orderID = getItem("ShopProductId") || "null";
//             let _payWay = PaymentManager.getInstance().getCurPaymentName();
//             sendData2Backgroud(PlayerBO.getInstance().UUID, 0, {OrderId: _orderID, PayWay:_payWay, From:_index});
//             ///////////////////////////////////////////////////////////////////////////////////
//             dotAFEvent("af_purchase");
//             dot(DotKey.FIRST_RECHARG_SUCCESS + "_" + _index);
//         }
//     }
// }

export function shopRememberOrNot(){
    return true;
    // return PlayerBO.getInstance().IsPayUser;
}
