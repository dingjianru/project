// import { GuideBO } from "../bo/GuideBO";
import { Sound } from "./Sound";

/** 装饰器函数,使用装饰器修饰点击事件 */
export const eventPlugin = function (target, property, descriptor) {
    let desF = descriptor.value;
    descriptor.value = function (...args) {
        checkEvent(args[0]) && desF.apply(this, args);
    };
    return descriptor;
}

let __$$timestamp: number;
function checkEvent(e: cc.Event): boolean {
    //300毫秒内不能连续点击
    if (e && __$$timestamp && performance.now() - __$$timestamp < 300)
        return false;
    __$$timestamp = performance.now();
    // Sound._playBG();
    //新手引导消息拦截
    // if (GuideBO.isStart) {
    //     if (!e) {
    //         cc.error("Failed to get guide event");
    //         return true;
    //     }
    //     if (e.target == GuideBO.target && GuideBO.isApplyFunc) {
    //         GuideBO.onTapPointer();
    //     } else {
    //         return false;
    //     }
    // }
    return true;
}

export class EventUtil {
    static addListener(target, eventType: string, listener: Function, thisObj?) {
        if (!target.events)
            target.events = {};
        target.events[eventType] = { callback: listener, thisObj: thisObj, eventType: eventType };
        target.on(eventType, EventUtil.touchEvent, thisObj);
    }

    static removeListener(target, eventType: string, listener: Function, thisObj?) {
        if (!target.events)
            return;
        var arg = target.events[eventType];
        if (!arg)
            return;

        var thisObj = arg.thisObj;
        arg.callback = arg.thisObj = null;
        delete target.events[eventType];

        target.off(eventType, listener, thisObj);
    }

    private static getTarget(target) {
        while (target && !target.events)
            target = target.parent;
        return target;
    }

    private static touchEvent(e: cc.Event) {
        var target = EventUtil.getTarget(e.target);
        if (!target)
            return false;
        var arg = target.events[e.type];

        if (!arg)
            return false;

        checkEvent(e) && arg.callback.call(arg.thisObj, e);

        return true;
    }
}