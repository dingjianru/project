let DeleteDelay = 15000;
export class GCManager {
	public constructor() {
		this.recycleList = {};
		this.quoteList = {};
		this.quotePool = [];
		this.totalSpace = 0;
		setInterval(this.onExecute.bind(this), 3000);
	}

	private recycleList: any;
	private quoteList: any;
	private quotePool: Array<Quote>;
	private totalSpace: number;
	private isStart: boolean = false;
	private pauseCount: number;

	public start() {
		this.isStart = true;
		this.pauseCount = 0;
	}

	public resume() {
		this.pauseCount--;
		if (this.pauseCount < 0)
			this.pauseCount = 0;
	}

	public pause() {
		this.pauseCount++;
	}

	public put(hashCode: string, key: string | string[], delay: number = DeleteDelay): void {
		let urls = Array.isArray(key) ? key : [key];
		for (let url of urls) {
			var q: Quote = this.quoteList[url];
			if (q == null) {
				q = this.getQuote(url);
				q.putHash(hashCode);
				q.key = url;
				q.reTime = delay;
				this.quoteList[url] = q;
			} else {
				q.putHash(hashCode);
			}
		}
	}

	private getQuote(url: string) {
		let q = null;
		if (!url)
			return new Quote();
		if (this.recycleList[url]) {
			q = this.recycleList[url];
			delete this.recycleList[url];
		} else if (this.quotePool.length)
			q = this.quotePool.pop();
		else
			q = new Quote();
		return q;
	}

	public remove(hashCode: string): void {
		for (let url in this.quoteList) {
			var q: Quote = this.quoteList[url];
			q.removeHash(hashCode);
			if (q.count <= 0) {
				delete this.quoteList[url];
				q.getTime = performance.now();
				this.recycleList[url] = q;
			}
		}
	}

	/**
	 * 根据资源url 删除不再引用中的资源
	 * @param urls 
	 */
	public removeByUrl(urls: string[]): void {
		for (let url in urls) {
			var q: Quote = this.quoteList[url];
			if (!q) {
				q = this.getQuote(url);
				q.key = url;
				q.reTime = DeleteDelay;
				q.getTime = performance.now();
				this.recycleList[url] = q;
			}
		}
	}

	private onExecute(): void {
		if (this.pauseCount || !this.isStart)
			return;
		var q: Quote;
		var now: number = performance.now();
		let quoteListKeys = Object.keys(this.quoteList);
		let recycleListKeys = Object.keys(this.recycleList);
		if (true) {
			//${quoteListKeys.map(value => value.substr(value.lastIndexOf("/") + 1))}
			//${recycleListKeys + ""}
			// console.log(`
			// 	/**
			// 	 * quotelist-> 
			// 	 * quoteLen-> ${quoteListKeys.length}
			// 	 * recycleList-> 
			// 	 * recycleLen-> ${recycleListKeys.length}
			// 	 * 
			// 	 */`);
		}
		for (var key in this.recycleList) {
			q = this.recycleList[key];
			if (now - q.getTime >= q.reTime) {
				cc.loader.release(key);
				delete this.recycleList[key];
				q.reset();
				this.quotePool.push(q);
			}
		}
	}

	private static _instance: GCManager;
	public static instance(): GCManager {
		if (!GCManager._instance) GCManager._instance = new GCManager();
		return GCManager._instance;
	}
}

class Quote {
	public constructor() {
		this.hashCodes = [];
	}
	public get count(): number { return this.hashCodes.length; };
	public key: string = "";
	public reTime: number = DeleteDelay;
	public getTime: number = 0;
	public space: number = 0;
	public tryTime: number = 0;
	private hashCodes: Array<string>;
	public putHash(code: string) { if (this.hashCodes.indexOf(code) == -1) this.hashCodes.push(code); }
	public removeHash(code: string): void {
		let index: number = this.hashCodes.indexOf(code);
		if (index != -1) this.hashCodes.splice(index, 1);
	}
	public reset(): void {
		this.key = "";
		this.reTime = DeleteDelay;
		this.getTime = 0;
		this.hashCodes = [];
		this.space = 0;
		this.tryTime = 0;
	}
}
