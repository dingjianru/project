import { getItem, StoreKey, setItem, getSoundSwitch, setSoundSwitch, getMusicSwitch, setMusicSwitch } from "./LocalStorage";
import { RES } from "./RES";

const Config = {
    PATH: "music",      // 资源路径
    // SWITCH: true,       // 默认声音开关
    SOUND: {            // 声音极其对应的url数据
        "TPBlind":"tp_chips_bet",               //下注	
        "TPDeal":"fapai",                       //发牌	
        "TPSee":"tp_pub_deal",                  //看牌
        "TPPacked":"tp_pub_pack",               //弃牌
        "TPWin":"win_me",                       //胜利	
        "Click":"button",
        "MainBg":"lobby_bg",                    //大厅背景音乐	
        "TPGameBg":"game",                      //游戏背景音乐
        "TPGetChips":"tp_chips_shoujinbi",      //赢的时候收金币
        "TPCtrlTime":"tp_pub_clock",            //操作前25s提醒
        "TPCtrlLast5":"tp_pub_countdown",       //操作后5s提醒
        "TPChaal":"tp_chaal",                   //下注
        "TPBlind2":"tp_blind",                  //闷牌
        "TPPack":"tp_pack",                     //弃牌
        "TPSide":"tp_side",                     //比牌
        "TPSideEffect":"tp_side_effect",        //比牌碰撞特效
    },
    SOUNDSWITCH: true,       // 默认声音开关
    MUSICSWITCH: true,       // 默认音乐开关
}

type TypeSound = keyof typeof Config.SOUND;


interface DataSoundInstance {
    url: string,
    loop?: boolean,
    volume?: number,
    // clip?: cc.AudioClip,
    id?: number,
}

class SoundMap {
    map: { [P in TypeSound]: DataSoundInstance };

    constructor() {
        this.map = {} as { [P in TypeSound]: DataSoundInstance };
    }

    set(key: TypeSound, data: DataSoundInstance) {
        this.map[key] = data;
    }

    get(key: TypeSound): DataSoundInstance {
        return this.map[key];
    }
}

export class Sound {

    private soundMap: SoundMap;

    private constructor() {
        this.soundMap = new SoundMap;
        for (let key in Config.SOUND) {
            this.soundMap.map[key] = { url: Config.SOUND[key], loop: false, volume: 1 };
        }
        this.soundMap.map.MainBg.loop = true;
        this.soundMap.map.TPGameBg.loop = true;
    }

    private static ins: Sound;

    private static init_local() {
        Config.SOUNDSWITCH = getSoundSwitch();
        Config.MUSICSWITCH = getMusicSwitch();
    }

    static init() {
        if (Sound.ins)
            return;
        Sound.ins = new Sound();
        this.init_local();
    }

    static getSoundSwitch(): boolean {
        return Config.SOUNDSWITCH;
    }

    static getMusicSwitch(): boolean {
        return Config.MUSICSWITCH;
    }

    static setSoundSwitch() {
        Config.SOUNDSWITCH = !Config.SOUNDSWITCH;
        setSoundSwitch(Config.SOUNDSWITCH);
        
    }

    static setMusicSwitch() {
        Config.MUSICSWITCH = !Config.MUSICSWITCH;
        setMusicSwitch(Config.MUSICSWITCH);
        if(Config.MUSICSWITCH){
            Sound.playBGMusic();
        }else{
            cc.audioEngine.uncacheAll();
        }
    }
    
    /**
     * 播放指定声音
     * @param sound 
     */
    static async play(sound: keyof typeof Config.SOUND) {
        let info = Sound.ins.soundMap.get(sound);
        // 载入audio clip资源
        let _clip = await RES.loadRes(`${Config.PATH}/${info.url}`, cc.AudioClip)
        // if (!info.clip) {
        //     info.clip = await RES.loadRes(`${Config.PATH}/${info.url}`, cc.AudioClip)
        // }
        if (!_clip) {
            cc.error("Failed to load Sound: " + info.url);
            return
        }
        if (info.loop) {
            cc.audioEngine.uncacheAll();
        }
        info.id = cc.audioEngine.play(_clip, info.loop, info.volume);
    }

    static flag = false;
    static _sound = null;
    static playBGMusic(sound: keyof typeof Config.SOUND = null) {
        if(sound == null && Sound._sound != null){
            sound = Sound._sound;
        }
        Sound._sound = sound;
        if (!Sound.ins)
            return;
        if(!Sound.getMusicSwitch())
            return;
        this.play(sound);
        // this.flag = true;
    }

    static playSound(sound:keyof typeof Config.SOUND){
        if (!Sound.ins)
            return;
        if(!Sound.getSoundSwitch())
            return;
        this.play(<keyof typeof Config.SOUND>sound);
    }
    /**
     * 停止某个声音
     * @param sound 
     */
    static stop(sound: keyof typeof Config.SOUND) {
        let info = Sound.ins.soundMap.get(sound)
        if (info.loop) {
            // bgm类型,pause
            cc.audioEngine.pause(info.id)
        } else {
            // 普通音效类型,stop
            cc.audioEngine.stop(info.id)
        }
    }

}