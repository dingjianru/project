const { property, ccclass } = cc._decorator;

export class MsgCenter {

    // /** 用在网络通讯 */
    // public static NetDispatcher = new cc.EventTarget();
    //
    // public static sendNetMsg(event: cc.Event.EventCustom) {
    //     MsgCenter.NetDispatcher.dispatchEvent(event);
    // }
    //
    // public static addNetMsg(type: com.boss.proto.Code | com.boss.proto.Sys, listener: Function, thisObject?: any) {
    //     MsgCenter.NetDispatcher.on(type + "", listener, thisObject);
    // }
    // public static removeNetMsg(type: com.boss.proto.Code, listener: Function, thisObject?: any) {
    //     MsgCenter.NetDispatcher.off(type + "", listener, thisObject);
    // }
    //
    // public static respones(e: cc.Event.EventCustom) {
    //     let msg = com.boss.proto.HPOperateSuccess.decode(e.getUserData());
    //     let event = new cc.Event.EventCustom(msg.code + "", false);
    //     console.log("=======HPOperateSuccess=========code = ", msg.code + "");
    //     event.setUserData(e.getUserData());
    //     MsgCenter.NetDispatcher.dispatchEvent(event);
    //
    //
    // }

    /** -----------------------------游戏内消息----------------------------------- */

    /** 用在游戏内部通讯 */
    public static GameDispatcher = new cc.EventTarget();

    // public static sendGameMsg(event: cc.Event) {
    //     MsgCenter.GameDispatcher.dispatchEvent(event);
    // }
    //带参事件
    public static emitGameEvent(eventType: string | number, data?:any,data2?:any,data3?:any,data4?:any,data5?:any){
        MsgCenter.GameDispatcher.emit(eventType.toString(),data,data2,data3,data4,data5);
    }

    public static addGameMsg(type: string | number, listener: Function, thisObject?: any) {
        MsgCenter.GameDispatcher.on(type.toString(), listener, thisObject);
    }

    public static removeGameMsg(type: string | number, listener: Function, thisObject?: any) {
        MsgCenter.GameDispatcher.off(type.toString(), listener, thisObject);
    }
    //关闭目标对象所有事件
    public static removeTargetEvent(thisObject: any){
        MsgCenter.GameDispatcher.targetOff(thisObject);
    }


    /** -----------------------------人物属性消息----------------------------------- */

    // /** 用在游戏内部通讯 */
    // public static PropDispatcher = new cc.EventTarget();
    //
    // public static sendPropMsg(event: cc.Event) {
    //     MsgCenter.PropDispatcher.dispatchEvent(event);
    // }
    //
    // public static addCPropMsg(type: com.boss.proto.CommonPropDeclare, listener: Function, thisObject?: any) {
    //     MsgCenter.PropDispatcher.on((type + 1000) + "", listener, thisObject);
    // }
    // public static addBPropMsg(type: com.boss.proto.BPropertyDeclare, listener: Function, thisObject?: any) {
    //     MsgCenter.PropDispatcher.on((type + 2000) + "", listener, thisObject);
    // }
    //
    // public static removeCPropMsg(type: com.boss.proto.CommonPropDeclare, listener: Function, thisObject?: any) {
    //     MsgCenter.PropDispatcher.off((type + 1000) + "", listener, thisObject);
    // }
    // public static removeBPropMsg(type: com.boss.proto.BPropertyDeclare, listener: Function, thisObject?: any) {
    //     MsgCenter.PropDispatcher.off((type + 2000) + "", listener, thisObject);
    // }
    //
    // public static hasPropListener(prop: string) {
    //     return this.PropDispatcher.hasEventListener(prop);
    // }
}