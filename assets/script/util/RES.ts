import { GCManager } from "./GCManager";
let downLoadMap = {};
let retryTime = 2;
export class RES {
    /**
     * 载入单个资源
     * @param path 
     * @param type 
     */
    static loadRes<T extends typeof cc.Asset>(path: string, type: T): Promise<InstanceType<T>> {
        GCManager.instance().pause();
        if (!downLoadMap[path])
            downLoadMap[path] = 1;
        else
            downLoadMap[path]++;
        return new Promise(res => {
            cc.loader.loadRes(path, type, async (err, resource) => {
                if (err) {
                    cc.warn(`load res fail, path=${path}, err=${err["errorMessage"]}`);
                    if (downLoadMap[path] < retryTime) {
                        res(await RES.loadRes(path, type));
                    } else {
                        GCManager.instance().resume();
                        res(null);
                        delete downLoadMap[path];
                    }
                } else {
                    GCManager.instance().resume();
                    res(resource);
                    delete downLoadMap[path];
                }
            })
        })
    }

    static loadResArray<T extends typeof cc.Asset>(path: string[], type: T): Promise<InstanceType<T>[]> {
        GCManager.instance().pause();
        let key = path.join(",");
        if (!downLoadMap[key])
            downLoadMap[key] = 1;
        else
            downLoadMap[key]++;
        return new Promise(res => {
            cc.loader.loadResArray(path, type, async (err, resource) => {
                if (err) {
                    cc.warn(`load res fail, path=${path}, err=${err["errorMessage"]}`);
                    if (downLoadMap[key] < retryTime) {
                        res(await RES.loadResArray(path, type));
                    } else {
                        GCManager.instance().resume();
                        res(null);
                        delete downLoadMap[key];
                    }
                } else {
                    GCManager.instance().resume();
                    res(resource);
                    delete downLoadMap[key];
                }
            })
        })
    }

    static gmCmd(cmd: string) {
        // let buf = com.boss.proto.GmInfoRequest.encode({ item: cmd });
        // Net.send(com.boss.proto.Code.GM_C, buf.finish());
    }
}

cc["RES"] = RES;