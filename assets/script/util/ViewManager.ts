
import { RES } from "./RES";
import { GCManager } from "./GCManager";

// import LoadingPanel from "../view/panel/LoadingPanel";
// import { GuideBO } from "../bo/GuideBO";
import { BaseView } from "../view/base/BaseView";
// import LoadingPanelPanel from "../view/common/LoadingPanelPanel";
// import GuidePanel from "../view/guide/GuidePanel";
// import { MainScene } from "../view/main/MainScene";

export const ViewLayer = {
    bottom: 0,
    middle: 1,
    top: 2,
    none: 3,
}

type LayerType = keyof typeof ViewLayer;

enum ViewState {
    open = 1,
    close,
}

interface ViewInstance {
    cls: typeof BaseView;
    // 静态部分
    prefab?: cc.Prefab;         // prefab
    // 数据部分
    state?: ViewState;   // 当前的数据状态
    z_index?: number;           // open时设定的node-z-index
    node?: cc.Node;             // 实例节点
}

interface Map<V> {
    [key: string]: V;
}


/** 装饰器函数,使用装饰器配置类 */


export class ViewManager {
    /**
     * 游戏层级分为上中下三层
     */
    private static layers: cc.Node[] = [];
    private static scene: cc.Scene;
    private static node: cc.Node;
    private static topNode:cc.Node;
    private static touchMask: cc.Node;

    private static loading: cc.Prefab;

    private static instanceMap: Map<ViewInstance> = {};

    /**
     * 使用场景初始化ViewManager，每次切换场景时都需要执行一次
     * @param scene 当前场景
     * @param bottomLayer 
     * @param middleLayer 
     * @param topLayer 
     */
    public static init(scene: cc.Scene , node:cc.Node) {
    // public static init(scene: cc.Scene, bottom: cc.Node, middle: cc.Node, top: cc.Node) {
        // ViewManager.layers = [];
        // ViewManager.layers.push(bottom);
        // ViewManager.layers.push(middle);
        // ViewManager.layers.push(top);
        if (ViewManager.scene) {
            GCManager.instance().remove(ViewManager.scene.uuid);
        }
        GCManager.instance().put(scene.uuid, scene["dependAssets"]);
        ViewManager.scene = scene;
        ViewManager.node = node;

        if (scene.name == "MainScene" || !scene.name){
            GCManager.instance().start();
        }
    }

    /**
     * 加载指定页面和资源，并根据layer放置在页面中
     * @param cls 页面对应的WrapperClass
     */
    public static async load<T extends typeof BaseView>(cls: T, params?: any, layer? : cc.Node): Promise<void> {
        // layer = layer || "middle";
        let instance = ViewManager.getInstance(cls);
        if (instance.state === ViewState.open) {
            cc.warn(cls["name"] + " has already opened!");
            return;
        }

        instance.state = ViewState.open;

        // if (cls as any != LoadingPanelPanel) {
        //     await ViewManager.load(LoadingPanelPanel);
        // } else {
        //     //加载其它界面前阻塞引导
        //     // GuideBO.block++;
        // }

        if (!instance.prefab || !instance.prefab.isValid) {
            instance.prefab = await RES.loadRes(cls.CONFIG.path, cc.Prefab);
        }
        if (!instance.prefab) {
            cc.error("找不到于界面对应的Prefab：" + cls.CONFIG.path);
            return
        }
        
        // if (!instance.node && layer != "none") {
        if (!instance.node) {
            instance.node = cc.instantiate(instance.prefab);
            if (!instance.node.getComponent(cls))
                instance.node.addComponent(cls);
            // if (cls as any != LoadingPanelPanel) {
            //     ViewManager.close(LoadingPanelPanel);
            // }
        }
        cc.log(">>>>>>>>>>>>>>>>show :" + instance.node.name)
        // ViewManager.node.width = 1385;
        let _parent = null;
        if(layer != undefined){
            _parent = layer;
        }else{
            _parent = ViewManager.node;
        }

        if (instance.node.name == "LoadingPanel"){
            _parent.addChild(instance.node, 999);
        }
        else if(instance.node.name == "AD_BannerTip"){
            _parent.addChild(instance.node, 998);
        }
        else{
            _parent.addChild(instance.node);
        }
        
        // layer != "none" && ViewManager.layers[ViewLayer[layer]].addChild(instance.node);
        params != undefined && instance.node.getComponent(cls).init && instance.node.getComponent(cls).init(params);

        // if (instance.node && instance.node.name != "LoadingPanel" && instance.node.name != "GuidePanel") {
        //     ViewManager.close(GuidePanel);
        // }
    }

    /**
     * 关闭指定页面，并删除有关资源
     * @param cls 
     */
    public static async close<T extends typeof BaseView>(cls: T, ignoreWarn: boolean = false): Promise<void> {
        let instance = ViewManager.getInstance(cls);
        if (!instance) {
            cc.error("No instance found : ", cls ? cls.CONFIG.path : "...");
            return;
        }
        let nodeName;
        if (instance.state === ViewState.close) {
            !ignoreWarn && cc.warn(cls["name"] + " has already closed!");
            return
        }
        instance.state = ViewState.close;
        cc.log("close name===", cls["name"]);
        if (instance.node) {
            nodeName = instance.node.name;
            instance.node["__$$closeByVM"] = true;
            instance.node.destroy()
            instance.node = null;
            instance.prefab = null;
            
        }
        let key = cls.CONFIG.path
        delete ViewManager.instanceMap[key];

        // if (nodeName == "LoadingPanel")
        //     return;

        // let childrenCount = 0;
        // for (let i = 0; i < this.layers.length; i++) {
        //     if (this.layers[i].children) {
        //         for (let j = 0; j < this.layers[i].children.length; j++) {
        //             childrenCount++;
        //         }
        //     }
        // }

        // if (childrenCount <= 1) {
        //     setTimeout(() => { GuideBO.checkViewGuide("Scene"); }, 100);
        // }
    }

    /**
     * 获取指定类实例
     * @param cls 类构造方法
     */
    public static get<T extends BaseView>(cls: { prototype: T, [key: string]: any } | string): T {
        let value;
        if (typeof cls == 'string') {
            if (cls == "Scene") {
                // return this.getScene(MainScene) as any;
            }
            value = ViewManager.instanceMap["prefab/panel/" + cls];
        } else {
            value = ViewManager.instanceMap[cls.CONFIG.path];
        }
        if (value && value.node) {
            return value.node.getComponent(cls);
        }
        return null;
    }

    /**
     * 获取当前场景对应脚本组建实例
     * @param cls 脚本类
     */
    public static getScene<T extends cc.Component>(cls: { prototype: T }): T {
        let scene = cc.director.getScene().children[0];
        return scene.getComponent(cls);
    }

    public static getTopLayer(): cc.Node {
        return ViewManager.node;
    }

    // public static getMiddleLayer(): cc.Node {
    //     return this.layers[1];
    // }

    // public static getBottomLayer(): cc.Node {
    //     return this.layers[0];
    // }

    //-----------------------------组件
    /**
     * 获取map实例
     * @param cls 
     */
    public static getInstance(cls: typeof BaseView): ViewInstance {
        if (!cls) {
            return null;
        }
        let key = cls.CONFIG.path
        let value = ViewManager.instanceMap[key];
        if (!value) {
            value = { cls: cls };
            this.instanceMap[key] = value;
        }
        return value
    }

    private static clearAllPanel(){
        
    }

    /**
     * 检查当前页面是否被存入了实例Map中
     * @param cls 
     */
    public static checkInstance(cls: typeof BaseView): boolean {
        let key = cls.CONFIG.path;
        return !!ViewManager.instanceMap[key];
    }

}
