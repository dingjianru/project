import { RES } from "./RES";
import { getRandom } from "./ToolUtil";

const {ccclass, property} = cc._decorator;

@ccclass
export class LocalImageLoader {
    public call = null;
    public async loadHeadImage(_sprite:cc.Sprite, _url:string, _userID, _size = 0, _sex = 0) {
        cc.log("------------->>>>>>>>>>>>load head url :" + _url + " player id: " + _userID);
        if(_url.length > 5 || _url == "-1"){//远程头像
            if(_url == "-1"){
                let _sexStr = _sex == 1 ? "female/" : "male/";
                let _key = 0;
                if(_sex == 1){
                    _key = (_userID % 560) + 1;
                }else{
                    _key = (_userID % 709) + 1;
                }
                _url = "http://www.rummydownload.com/rummyroseway/head/" + _sexStr + _key + ".jpg";
            }else{
                _url = "https://graph.facebook.com/" + _url + "/picture?type=small";
            }
            if(cc.sys.isMobile)
            {
                this.imageLoadTool(_url, _userID, _sprite, _size);
                return;
            }
        }else{//本地头像
            let res = await RES.loadRes("headImage/img_head_" + _url, cc.SpriteFrame);
            if (res && _sprite.isValid){
                _sprite.spriteFrame = res;
                this._call(res);
            }else{
                
            }
        }
        cc.log("loadHeadImage："+_url)
    }

    private _call(spriteFrame){
        if(this.call){
            this.call(spriteFrame)
        }
    }
    
    private imageLoadTool(url:string, uesrId:string,  sp:cc.Sprite, size = 0){
        cc.log(">>>>>>>>>>>>>>>>>>>>>>>load head url: " + url)
        var dirpath =  jsb.fileUtils.getWritablePath() + 'headImage/';
        var filepath = dirpath + uesrId + '.jpg';
        let self = this;
        function loadEnd(){
            cc.loader.load(filepath, function(err, tex){
                if( err ){
                    cc.error(err);
                }else{
                    var spriteFrame = new cc.SpriteFrame(tex);
                    if( spriteFrame && sp.isValid){
                        sp.spriteFrame = spriteFrame;
                        self._call(spriteFrame);
                        if(size > 0){
                            sp.node.height = size;
                            sp.node.width = size;
                        }
                    }else{
                        cc.log(">>>>>>>>>>>>>>>>>>>>>>>sp.isValid")
                    }
                }
            });
        }

        if( jsb.fileUtils.isFileExist(filepath) ){
            cc.log('Remote is find' + filepath);
            loadEnd();
            return;
        }

        var saveFile = function(data){
            if( typeof data !== 'undefined' ){
                if( !jsb.fileUtils.isDirectoryExist(dirpath) ){
                    jsb.fileUtils.createDirectory(dirpath);
                }else{
                    cc.log("路径exist");
                }
                // new Uint8Array(data) writeDataToFile  writeStringToFile
                if( jsb.fileUtils.writeDataToFile( new Uint8Array(data) , filepath) ){
                    cc.log('Remote write file succeed.');
                    loadEnd();
                }else{
                    cc.log('Remote write file failed.');
                }
            }else{
                cc.log('Remote download file failed.');
            }
        };
        
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = function () {
            cc.log("xhr.readyState  " + xhr.readyState);
            cc.log("xhr.status  " + xhr.status);
            if (xhr.readyState === 4 ) {
                if(xhr.status === 200){
                    //responseType一定要在外面设置
                    // xhr.responseType = 'arraybuffer'; 
                    saveFile(xhr.response);
                }else{
                    saveFile(null);
                }
            }
        }.bind(this);
        //responseType一定要在外面设置
        xhr.responseType = 'arraybuffer';
        xhr.timeout = 7000;
        xhr.open("GET", url, true);
        xhr.send();
    }
}
