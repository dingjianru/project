/*
* 加载本地静态表 v: 1.0
* 表结构开头字段必须是id
* */

/*
* tableMap 用于缓存数据对象
* tableMap[key]
*
* 获取test.json表数据=>tableMap["test"]
* */
import {clone, series} from "../util/ToolUtil";

let tableMap = {};

/*
 * 加载文件
 * @param table 文件名
 * */
let loadTableFile = function (table, callback) {

    let pathName = "tables/" + table;

    cc.loader.loadRes(pathName, function (error, data) {
        if (error) {
           console.error(table + ".json 加载文件出错." + pathName +" "+JSON.stringify(error));
        } else {
            let _data = JSON.parse(JSON.stringify(data));
            tableMap[table] = _data.json;
            cc.log(table+" 配置加载完成！");
            callback && callback();
        }
    });

};


export class TableData {



    private static isLoading = false;
    private static isComplete = false;


    /*
    *   初始化数据
    * */
    static initTableFile (allTable,callback) {

        let tables = allTable;
        if(tables.length <= 0)
        {
            TableData.isComplete = true;
            TableData.isLoading = false;
            if (callback){
                callback(true);
            }
            return;
        }


        if (TableData.isLoading || TableData.isComplete) {
            if (callback){
                callback(true);
            }
            return;
        }

        //清空表数据
        tableMap = {};
        ////////////加载表开始/////////////

        let tasks = tables.map((tableName) => {
            return (done) => {
                loadTableFile(tableName, done);
            };
        });
        series(tasks, () => {
            TableData.isComplete = true;
            TableData.isLoading = false;

            if (callback){
                callback(true);
            }
            // console.log("加载数据表完成。");
        });
        TableData.isLoading = true;
        TableData.isComplete = false;
    }

    static getIsComplete(){
        return TableData.isComplete;
    }

    /*
    * 返回全表数据
    * @param key 表名
    * */

    static getTableForKey (key) {
        if(!tableMap[key])
            console.error("没有查找到本"+key+"数据！");
        return clone(tableMap[key]);
    }

    /*
    *   通过表名 和ID 获本表本ID数据
    *   @param key 表名
    *   @param ID   表数据索引
    * */
    static getDataForId (key, id) {

        let table = TableData.getTableForKey(key);
        if(table)
        {
            let data = table[id];
            if(!data)
                console.error(key+"表里没有找到ID为"+id+"的数据");
            return data;
        }
        console.error(key+"没有找到名为"+key+"的表！");
    }

    /*
    * 获取对象属性名称列表
    *   @param key 表名
    *   @return {Array}
    * */
    static getTableNames (key) {
        if(!tableMap[key])
            console.error("没有查找到本"+key+"数据！");
        let names = Object.getOwnPropertyNames(tableMap[key]);
        return names;
    }

    /*
    * 获取表数据长度
    * */
    static getTableSize (key) {

        let names = TableData.getTableNames(key);
        if(names)
        {
            return names.length;
        }
        return 0;
    }
}