export let CTRL_DIRECT = cc.Enum({
    NULL:0,
    LEFT:1,
    RIGHT:2,
    TOP:3,
    BOTTOM:4
})

export let WAY_TYPE = cc.Enum({
    FORBID: 0,
    ALLOW:1,
})

export let FLOOR_TYPE = cc.Enum({
    BASE: 0,
    TOP: 1,
})

export let OBJ_TYPE = cc.Enum({
    NULL:0,
    LAND:1,
    BUILD:2,
})

export let GRID_TYPE = cc.Enum({
    NULL: 0,
    STAY: 1,                //阻挡物
    TU_KUAI:2,              //土块
    TU_ZHUAN:3,             //土转
    // SHI_KUAI:4,             //石块
    // SHI_ZHUAN:5,            //石砖
    DI_LAO_ZHUAN:6,         //地牢砖

    LU_ZHANG:7,
    JIAN_TA:8,
    DIAN_TA:9,
    YAO_SAI:10,
    LIE_YAN:11,
    ZHAN_QI:12,

    RIVER:50,      //河
})

export let GRID_DATA = {
    0 : { 
            0:{//FLOOR_TYPE.BOTTOM
                URL:[], 
                PER:[]
            }, 
            1:{//FLOOR_TYPE.TOP
                URL:[], 
                PER:[]
            },
            "type":OBJ_TYPE.LAND
        },

    1 : {   //阻挡物
            0:{//FLOOR_TYPE.BOTTOM
                URL:['build3090', 'build3091', 'build3092', 'build3093', 'build3094'], 
                PER:[40,30,20,5,5]
            }, 
            1:{//FLOOR_TYPE.TOP
                URL:['build2090', 'build2091', 'build2092', 'build2093', 'build2094'], 
                PER:[40,30,20,5,5]
            }
        },

    2 : {
            0:{//FLOOR_TYPE.BOTTOM
                URL:[], 
                PER:[]
            }, 
            1:{//FLOOR_TYPE.TOP
                URL:[], 
                PER:[]
            }
        },

    3 : {   //土转
            0:{//FLOOR_TYPE.BOTTOM
                URL:['build3020', 'build3021', 'build3022', 'build3023', 'build3024'], 
                PER:[40,30,20,5,5]
            }, 
            1:{//FLOOR_TYPE.TOP
                URL:['build3020'], 
                PER:[100]
            },
            "type":OBJ_TYPE.LAND
        },
    
    4 : {
            0:{//FLOOR_TYPE.BOTTOM
                URL:[], 
                PER:[]
            }, 
            1:{//FLOOR_TYPE.TOP
                URL:[], 
                PER:[]
            }
        },
    5 : {
            0:{//FLOOR_TYPE.BOTTOM
                URL:[], 
                PER:[]
            }, 
            1:{//FLOOR_TYPE.TOP
                URL:[], 
                PER:[]
            }
        },
    6 : {   //地牢砖
            0:{//FLOOR_TYPE.BOTTOM
                URL:['build3090', 'build3091', 'build3092', 'build3093', 'build3094'], 
                PER:[40,30,20,5,5]
            }, 
            1:{//FLOOR_TYPE.TOP
                URL:['build2090', 'build2091', 'build2092', 'build2093', 'build2094'], 
                PER:[40,30,20,5,5]
            }
        },
    7 : {   //路障
        1:{//FLOOR_TYPE.TOP
            URL:['build1042'], 
            PER:[100]
        },
        "type":OBJ_TYPE.BUILD
    },
    
    50 : {  //河
            0:{//FLOOR_TYPE.BOTTOM
                URL:['build3900', 'build3901', 'build3902', 'build3903', 'build3904'], 
                PER:[100,30,20,5,5]
            }, 
            1:{//FLOOR_TYPE.TOP
                URL:[], 
                PER:[]
            }
        },
}