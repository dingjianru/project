import { BaseUI } from "./base/BaseUI";
//寻路
export default class NavmeshNode extends BaseUI {
    private _map = [];
    /**
     * 寻路方法
     * @param {Array} map 地图数据
     * @param {Array} start 起点 例如：[0, 0]
     * @param {Array} end 终点 例如：[50, 50]
     * @return Boolean
     */
    private path(map, start, end) {
        var queue = [start];
        function insert(x, y) {
            // 到达底盘边缘，直接停止
            if (x < 0 || x >= 100 || y < 0 || y >= 100) return;
            // 遇到地图的墙，也停止
            if (map[y * 100 + x]) return;
            // 标记可以走的路的格子的状态为 2
            map[y * 100 + x] = 2;
            // 把可走的路推入队列
            queue.push([x, y]);
        }
    
        // 循环格子 4 边的格子
        while (queue.length) {
        let [x, y] = queue.shift();
        console.log(x, y);
    
        // 遇到了终点位置就可以返回了
        if (x === end[0] && y === end[1]) return true;
            // 把上下左右推入队列
            insert(x - 1, y);
            insert(x, y - 1);
            insert(x + 1, y);
            insert(x, y + 1);
        }
        return false;
    }
}   