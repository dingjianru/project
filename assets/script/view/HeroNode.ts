import { GridHeight, GridWidth } from "../config/GameConfig";
import { CTRL_DIRECT } from "../config/GameEnum";
import { GameEvent } from "../config/GameEvent";
import { MsgCenter } from "../util/MsgCenter";
import { BaseUI } from "./base/BaseUI";

export default class HeroNode extends BaseUI {

    private _curSpeed = 0;
    private _maxSpeed = 200;
    private _onKeyUp = true;
    private _direction = CTRL_DIRECT.NULL;

    onRegistEvent(){
        MsgCenter.addGameMsg(GameEvent.EVENT_KEY_UP, this._onEventKeyUp, this);
        MsgCenter.addGameMsg(GameEvent.EVENT_KEY_DOWN, this._onEventKeyDown, this);
    }

    private _onEventKeyUp(){
        this._onKeyUp = true;
    }

    private  _onEventKeyDown(_direction){
        this._onKeyUp = false;
        this._direction = _direction;
        this._curSpeed = this._maxSpeed;
    }

    update(dt){
        if(this._curSpeed > 0){
            let _posX = this.node.x, _posY = this.node.y;
            switch(this._direction){
                case CTRL_DIRECT.LEFT:
                    _posX = this.node.x - this._curSpeed * dt;
                    if(this._onKeyUp){
                        let _end = Math.floor(_posX / GridWidth)
                        let _start = Math.floor(this.node.x / GridWidth);
                        if(_end != _start){
                            if(this.node.x > 0){
                                _posX = this.node.x - this.node.x % GridWidth;    
                            }else{
                                _posX = this.node.x - this.node.x % GridWidth - GridWidth;
                            }
                            this._curSpeed = 0;
                        }
                    }
                break;
                case CTRL_DIRECT.RIGHT:
                    _posX = this.node.x + this._curSpeed * dt;
                    if(this._onKeyUp){
                        let _end = Math.floor(_posX / GridWidth)
                        let _start = Math.floor(this.node.x / GridWidth)
                        if(_end != _start){
                            if(this.node.x > 0){
                                _posX = this.node.x + (GridWidth - this.node.x % GridWidth);
                            }else{
                                _posX = this.node.x - this.node.x % GridWidth;
                            }
                            this._curSpeed = 0;
                        }
                    }   
                break;
                case CTRL_DIRECT.TOP:
                    _posY = this.node.y + this._curSpeed * dt;
                    if(this._onKeyUp){
                        let _end = Math.floor(_posY / GridHeight)
                        let _start = Math.floor(this.node.y / GridHeight)
                        if(_end != _start){
                            if(this.node.y > 0){
                                _posY = this.node.y + (GridHeight - this.node.y % GridHeight);
                            }else{
                                _posY = this.node.y - this.node.y % GridHeight;
                            }
                            this._curSpeed = 0;
                        }
                    }   
                break;
                case CTRL_DIRECT.BOTTOM:
                    _posY = this.node.y - this._curSpeed * dt;
                    if(this._onKeyUp){
                        let _end = Math.floor(_posY / GridHeight)
                        let _start = Math.floor(this.node.y / GridHeight);
                        if(_end != _start){
                            if(this.node.y > 0){
                                _posY = this.node.y - this.node.y % GridHeight;    
                            }else{
                                _posY = this.node.y - this.node.y % GridHeight - GridHeight;
                            }
                            this._curSpeed = 0;
                        }
                    }
                break;
            }
            this.node.setPosition(_posX, _posY);
            // this.camera.node.setPosition(this.HeroItem.x, this.HeroItem.y);
        }
    }
}   
