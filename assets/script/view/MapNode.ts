import { CTRL_DIRECT, FLOOR_TYPE, GRID_DATA, GRID_TYPE, WAY_TYPE } from "../config/GameEnum";
import { RES } from "../util/RES";
import { BaseView, classConfig } from "./base/BaseView";

const {ccclass, property} = cc._decorator;

@ccclass
@classConfig("MapNode")
export default class MapNode extends BaseView {
    @property(cc.Node)
    mapNode: cc.Node = null;
    @property(cc.Node)
    topNode: cc.Node = null;
    @property(cc.Node)
    bottomNode: cc.Node = null;
    @property(cc.Node)
    GridItem: cc.Node = null;
    @property(cc.Node)
    HeroItem: cc.Node = null;
    @property(cc.Node)
    GridBottomNode: cc.Node = null;
    @property(cc.Camera)
    camera: cc.Camera = null;

    private _mapWidthCount = 17;
    private _mapHeightCount = 11;
    private _gridWidth = 50;
    private _gridHeight = 50;
    //////////////////////////////////////////////////////////////
    private _baseFloorMap = []; 
    private _topFloorMap = [];
    private _baseFloorMapResource = [];
    private _topFloorMapResource = [];
    private _mainWayMap = [];//寻路用地图
    //////////////////////////////////////////////////////////////

    start(){
        this._initMapData();
        this._initMap();
    }

    private _initMapData(){
        for(let i = 0 ; i < this._mapHeightCount; i++ ){
            this._baseFloorMap[i] = [];
            this._topFloorMap[i] = [];
            this._baseFloorMapResource[i] = [];
            this._topFloorMapResource[i] = [];
            this._mainWayMap[i] = [];
        }
        for(let i = 0 ; i < this._mapHeightCount; i++ ){
            for(let j = 0 ; j < this._mapWidthCount; j++ ){
                if(i == 0 || i == this._mapHeightCount - 1 || j == 0 || j == this._mapWidthCount - 1){
                    this._baseFloorMap[i][j] = GRID_TYPE.STAY;
                    this._topFloorMap[i][j] = GRID_TYPE.STAY;
                    this._baseFloorMapResource[i][j] = 0;
                    this._topFloorMapResource[i][j] = 0;
                }else if(i == 1 || i == 3 || i == 5){
                    this._baseFloorMap[i][j] = GRID_TYPE.RIVER;
                    this._topFloorMap[i][j] = GRID_TYPE.NULL;
                    this._baseFloorMapResource[i][j] = this._getSuitIndexByRandom(GRID_TYPE.RIVER, FLOOR_TYPE.BASE);
                    this._topFloorMapResource[i][j] = 0;
                }else{
                    this._baseFloorMap[i][j] = GRID_TYPE.TU_ZHUAN;
                    this._topFloorMap[i][j] = GRID_TYPE.NULL;
                    this._baseFloorMapResource[i][j] = this._getSuitIndexByRandom(GRID_TYPE.TU_ZHUAN, FLOOR_TYPE.BASE);
                    this._topFloorMapResource[i][j] = 0;
                }
                this._mainWayMap[i][j] = WAY_TYPE.FORBID;
            }
        }
        cc.log(this._baseFloorMap)
    }

    private _initMap(){
        for(let i = 0 ; i < this._mapHeightCount; i++ ){
            for(let j = 0 ; j < this._mapWidthCount; j++ ){
                this._initGrid(i , j);
            }
        }
    }

    private _initGrid(i, j){
        this._initGridBottom(i, j);
        this._initGridTop(i, j);
    }

    private _initGridBottom(i, j){
        let _gridData = this._baseFloorMap[i][j];
        if(_gridData != GRID_TYPE.NULL){
            let _node = cc.instantiate(this.GridBottomNode);
            this._initGridItem(_node, i, j, FLOOR_TYPE.BASE);
            _node.parent = this.bottomNode;
            _node.y = ( Math.floor(this._mapHeightCount / 2) - i) * this._gridHeight;
            _node.x = -( Math.floor(this._mapWidthCount / 2) - j ) * this._gridWidth;
        }
    }

    private _initGridTop(i, j){
        let _gridData = this._topFloorMap[i][j];
        if(_gridData != GRID_TYPE.NULL){
            let _node = cc.instantiate(this.GridItem);
            this._initGridItem(_node, i, j, FLOOR_TYPE.TOP);
            _node.parent = this.topNode;
            _node.y = ( Math.floor(this._mapHeightCount / 2) - i ) * this._gridHeight;
            _node.x = -( Math.floor(this._mapWidthCount / 2) - j ) * this._gridWidth;
        }
    }

    private _build(_type, i, j){
        if(this._baseFloorMap[i][j] != GRID_TYPE.NULL && this._baseFloorMap[i][j] != GRID_TYPE.RIVER && this._topFloorMap[i][j] == GRID_TYPE.NULL){
            this._topFloorMap[i][j] = _type;
            this._topFloorMapResource[i][j] = this._getSuitIndexByRandom(_type, FLOOR_TYPE.TOP);
            this._mainWayMap[i][j] = WAY_TYPE.FORBID;
        }
    }
    private async _initGridItem(_node:cc.Node, _x, _y, _type){
        let _gridData = GRID_TYPE.NULL;
        if(_type == FLOOR_TYPE.TOP){
            _gridData = this._topFloorMap[_x][_y];
        }else{
            _gridData = this._baseFloorMap[_x][_y];
        }
        if(_gridData != GRID_TYPE.NULL){
            let _url = "";
            if(_type == FLOOR_TYPE.TOP){
                _url = GRID_DATA[_gridData][_type].URL[this._topFloorMapResource[_x][_y]];
            }else{
                _url = GRID_DATA[_gridData][_type].URL[this._baseFloorMapResource[_x][_y]];
            }
            let _sprite = _node.getComponentInChildren(cc.Sprite);
            let res = await RES.loadRes("image/" + _url, cc.SpriteFrame);
            if (res)_sprite.spriteFrame = res;
        }
    }

    private _getSuitIndexByRandom(_type, _floor){
        let _total = 0;
        let _value = Math.random() * 100;
        let _randoms = GRID_DATA[_type][_floor].PER;
        for(let i = 0 ; i < _randoms.length; i++){
            _total = _total + _randoms[i];
            if(_total >= _value)return i;
        }
        return 0;
    }
    /////////////////////////////////////////
    onRegistEvent(){
        cc.systemEvent.on('keydown', this.onKeyDown, this);
        cc.systemEvent.on('keyup', this.onKeyUp, this);
    }

    private _curSpeed = 0;
    private _maxSpeed = 200
    private _onKeyUp = true;
    private _direction = CTRL_DIRECT.NULL;

    onKeyDown( evt : cc.Event.EventKeyboard){
        switch(evt.keyCode){
            case cc.macro.KEY.a:
                this._direction = CTRL_DIRECT.LEFT;
                this._onKeyUp = false;
                this._curSpeed = this._maxSpeed;
            break;
            case cc.macro.KEY.d:
                this._direction = CTRL_DIRECT.RIGHT;
                this._onKeyUp = false;
                this._curSpeed = this._maxSpeed;
            break;
            case cc.macro.KEY.w:
                this._direction = CTRL_DIRECT.TOP;
                this._onKeyUp = false;
                this._curSpeed = this._maxSpeed;
            break;
            case cc.macro.KEY.s:
                this._direction = CTRL_DIRECT.BOTTOM;
                this._onKeyUp = false;
                this._curSpeed = this._maxSpeed;
            break;
            case cc.macro.KEY.q:

            break;
            default:
                return;
        }
    }
    
    onKeyUp( evt : cc.Event.EventKeyboard){
        switch(evt.keyCode){
            case cc.macro.KEY.a:
            case cc.macro.KEY.d:
            case cc.macro.KEY.w:
            case cc.macro.KEY.s:
                this._onKeyUp = true;
            break;
            default:
                return;
        }
    }

    update(dt){
        if(this._curSpeed > 0){
            let _posX = this.HeroItem.x, _posY = this.HeroItem.y;
            switch(this._direction){
                case CTRL_DIRECT.LEFT:
                    _posX = this.HeroItem.x - this._curSpeed * dt;
                    if(this._onKeyUp){
                        let _end = Math.floor(_posX / this._gridWidth)
                        let _start = Math.floor(this.HeroItem.x / this._gridWidth);
                        if(_end != _start){
                            if(this.HeroItem.x > 0){
                                _posX = this.HeroItem.x - this.HeroItem.x % this._gridWidth;    
                            }else{
                                _posX = this.HeroItem.x - this.HeroItem.x % this._gridWidth - this._gridWidth;
                            }
                            this._curSpeed = 0;
                        }
                    }
                break;
                case CTRL_DIRECT.RIGHT:
                    _posX = this.HeroItem.x + this._curSpeed * dt;
                    if(this._onKeyUp){
                        let _end = Math.floor(_posX / this._gridWidth)
                        let _start = Math.floor(this.HeroItem.x / this._gridWidth)
                        if(_end != _start){
                            if(this.HeroItem.x > 0){
                                _posX = this.HeroItem.x + (this._gridWidth - this.HeroItem.x % this._gridWidth);
                            }else{
                                _posX = this.HeroItem.x - this.HeroItem.x % this._gridWidth;
                            }
                            this._curSpeed = 0;
                        }
                    }   
                break;
                case CTRL_DIRECT.TOP:
                    _posY = this.HeroItem.y + this._curSpeed * dt;
                    if(this._onKeyUp){
                        let _end = Math.floor(_posY / this._gridHeight)
                        let _start = Math.floor(this.HeroItem.y / this._gridHeight)
                        if(_end != _start){
                            if(this.HeroItem.y > 0){
                                _posY = this.HeroItem.y + (this._gridHeight - this.HeroItem.y % this._gridHeight);
                            }else{
                                _posY = this.HeroItem.y - this.HeroItem.y % this._gridHeight;
                            }
                            this._curSpeed = 0;
                        }
                    }   
                break;
                case CTRL_DIRECT.BOTTOM:
                    _posY = this.HeroItem.y - this._curSpeed * dt;
                    if(this._onKeyUp){
                        let _end = Math.floor(_posY / this._gridHeight)
                        let _start = Math.floor(this.HeroItem.y / this._gridHeight);
                        if(_end != _start){
                            if(this.HeroItem.y > 0){
                                _posY = this.HeroItem.y - this.HeroItem.y % this._gridHeight;    
                            }else{
                                _posY = this.HeroItem.y - this.HeroItem.y % this._gridHeight - this._gridHeight;
                            }
                            this._curSpeed = 0;
                        }
                    }
                break;
            }
            this.HeroItem.setPosition(_posX, _posY);
            this.camera.node.setPosition(this.HeroItem.x, this.HeroItem.y);
        }
    }
}   