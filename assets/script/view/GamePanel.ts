import { GameManager } from "../util/GameManager";
import { ViewManager } from "../util/ViewManager";
import { BaseView, classConfig } from "./base/BaseView";

const {ccclass, property} = cc._decorator;

@ccclass
@classConfig("GamePanel")
export default class GamePanel extends BaseView {
    @property([cc.Label])
    propertyLabels: Array<cc.Label> = [];
   
}