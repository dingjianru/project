import { ViewManager } from "../../util/ViewManager";
import { BaseComponent } from "./BaseComponent";

const { ccclass, property } = cc._decorator;
@ccclass
export class BaseScene extends BaseComponent {
    // @property(cc.Node)
    // bottom: cc.Node = null;

    // @property(cc.Node)
    // middle: cc.Node = null;

    // @property(cc.Node)
    // top: cc.Node = null;

    /**
     * 初始化ViewManger
     */
    start() {
        // if (cc.sys.platform == cc.sys.IPAD){
        //     cc.Canvas.instance.fitHeight = true;
        // }
        ViewManager.init(cc.director.getScene(), this.node);
        // ViewManager.init(cc.director.getScene(), this.bottom, this.middle, this.top);

        // let node = this.top;
        // node._touchListener.swallowTouches = false;
        // this.iphoneXChangeWidget(this.node.getChildByName("Top"));
        // this.iphoneXChangeWidget(this.node.getChildByName("leftTop"));
        // this.iphoneXChangeWidget(this.node.getChildByName("rightTop"));
    }

    iphoneXChangeWidget(_node){
        // if (cc.sys.isMobile && NativeToJsFun.checkIsIphoneX()){
        //     this.checkWidget(_node);
        // }
    }

    checkIphoneX(name){
        let node = this.node.getChildByName(name);
        if (node)this.checkWidget(node); 
    }
    
    checkWidget(_node){
        if(_node){
            let widget = _node.getComponent(cc.Widget);
            if (widget) {
                widget.top += 88;
            }
        }
    }
    onLoad() {
        let srcScaleForShowAll = Math.min(cc.view.getCanvasSize().width / this.node.width, cc.view.getCanvasSize().height / this.node.height);
        let realWidth = this.node.width * srcScaleForShowAll;
        let realHeight = this.node.height * srcScaleForShowAll;
        // 节点宽高适配
        this.node.width = this.node.width * (cc.view.getCanvasSize().width / realWidth);
        this.node.height = this.node.height * (cc.view.getCanvasSize().height / realHeight);
    }
}