const { ccclass, property } = cc._decorator;

@ccclass
export class BaseUI extends cc.Component {
    onLoad(){
        this.onRegistEvent();
        this.onUpdateLabels();
    }

    onDestroy() {
    }

    //多语言刷新
    protected onUpdateLabels(){

    }
    //注册事件
    protected onRegistEvent(){

    }
}