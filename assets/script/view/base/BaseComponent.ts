import { RES } from "../../util/RES";
import { GCManager } from "../../util/GCManager";
import { BaseUI } from "./BaseUI";
const { ccclass, property } = cc._decorator;

@ccclass
export class BaseComponent extends BaseUI {

    constructor() {
        super();
    }
    /**
     * 加载资源
     * @param path 资源基于Resources目录下的相对路径 
     * @param type 资源类型，cc.Asset的子类型
     * @param cb 回调函数,返回请求的资源，如果资源为null，则表示请求失败
     */
    public async load(path: string, type: typeof cc.Asset, cb: (data) => void) {
        let res = await RES.loadRes(path, type);
        if (res && this.isValid) {
            GCManager.instance().put(this.node.uuid, cc.loader.getDependsRecursively(res));
        } else {
            res && GCManager.instance().removeByUrl(cc.loader.getDependsRecursively(res));
        }
        if (res && this.isValid) {
            cb(res);
        } else {
            if (this.isValid) {
                console.error("请求加载资源失败！" + path);
            }
        }
    }

    public async loadPromisify(path: string, type: typeof cc.Asset): Promise<any> {
        return RES.loadRes(path, type).then((data) => {
            if (data && this.isValid) {
                GCManager.instance().put(this.node.uuid, cc.loader.getDependsRecursively(data));
            } else {
                data && GCManager.instance().removeByUrl(cc.loader.getDependsRecursively(data));
            }
            return data;
        })
    }
    /**
     * 一次读取多个资源
     * @param path 资源url数组
     * @param type 
     * @param cb 
     */
    public async loadResArray(path: string[], type: typeof cc.Asset, cb: (data) => void) {
        let res = await RES.loadResArray(path, type);
        if (res && this.isValid) {
            res.forEach(asset => GCManager.instance().put(this.node.uuid, cc.loader.getDependsRecursively(asset)));
        } else {
            res && res.forEach(asset => GCManager.instance().removeByUrl(cc.loader.getDependsRecursively(asset)));
        }

        if (res && this.isValid) {
            cb(res);
        } else {
            if (this.isValid) {
                console.error("请求加载资源失败！" + path);
            }
        }
    }
}