
import { GCManager } from "../../util/GCManager";
import { BaseComponent } from "./BaseComponent";

const { ccclass, property } = cc._decorator;

interface ItemConfig {
    path: string;       // 资源路径;同时也作为唯一key使用
}

export const itemConfig = (path: string, type?) => {
    return (constructor: typeof BaseItem) => {
        constructor.CONFIG = {
            path: "prefab/" + (path || ""),
        }
        Object.freeze(constructor.CONFIG);
    }
}

@ccclass
export class BaseItem extends BaseComponent {

    static CONFIG: ItemConfig;

    onLoad() {
        GCManager.instance().put(this.node.uuid, cc.loader.getDependsRecursively(cc.loader.getRes(this.constructor["CONFIG"].path, cc.Prefab)));
    }

    onDestroy() {
        super.onDestroy();
        GCManager.instance().remove(this.node.uuid);
    }
}