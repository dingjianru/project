import { GCManager } from "../../util/GCManager";
import { ViewManager } from "../../util/ViewManager";
import { BaseComponent } from "./BaseComponent";

const { ccclass, property } = cc._decorator;

interface ViewConfig {
    path: string;       // 资源路径;同时也作为唯一key使用
    // type: TypeOpen;     // 打开方式
}

/** 装饰器函数,使用装饰器配置类 */
export const classConfig = (path: string, type?) => {
    return (constructor: typeof BaseView) => {
        constructor.CONFIG = {
            path: "prefab/" + (path || ""),
            // type: type || "single",
        }
        Object.freeze(constructor.CONFIG);
    }
}

@ccclass
export class BaseView extends BaseComponent {

    static CONFIG: ViewConfig;

    constructor() {
        super();
    }

    public init(params?: any) {

    }

    onLoad() {
        //兼容直接使用预制体创建，没有设置ViewManager状态
        super.onLoad();
        if (!ViewManager.checkInstance(this.constructor as any)) {
            // ViewManager.load(this.constructor as any, "none");
            return;
        }
        let instance = ViewManager.getInstance(this.constructor as any);
        if (instance.node && instance.prefab) {
            GCManager.instance().put(instance.node.uuid, cc.loader.getDependsRecursively(instance.prefab));
        }

        // this.load("prefab/item/TouchMask", cc.Prefab, (data) => {
        //     if (data && this.isValid) {
        //         let touchMask = cc.instantiate(data);
        //         touchMask.parent = this.node;
        //         touchMask.name = "touchMask";
        //         touchMask.active = true;
        //         touchMask.zIndex = -1;
        //     }
        // })
    }

    onDestroy() {
        super.onDestroy();
        GCManager.instance().remove(this.node.uuid);
        //再次调用close方法，防止直接移除界面，没有设置对应状态
        if (!this.node["__$$closeByVM"] && ViewManager.checkInstance(this.constructor as any))
            ViewManager.close(this.constructor as any, true);
    }
}