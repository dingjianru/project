import { CTRL_DIRECT, FLOOR_TYPE, GRID_DATA, GRID_TYPE, WAY_TYPE } from "../config/GameEnum";
import { GameEvent } from "../config/GameEvent";
import { MsgCenter } from "../util/MsgCenter";
import { BaseUI } from "./base/BaseUI";

export default class KeyBoardEnterNode extends BaseUI {

    private _direction = CTRL_DIRECT.NULL;

    onRegistEvent(){
        cc.systemEvent.on('keydown', this.onKeyDown, this);
        cc.systemEvent.on('keyup', this.onKeyUp, this);
    }

    onKeyDown( evt : cc.Event.EventKeyboard){
        switch(evt.keyCode){
            case cc.macro.KEY.a:
                this._direction = CTRL_DIRECT.LEFT;
            break;
            case cc.macro.KEY.d:
                this._direction = CTRL_DIRECT.RIGHT;
            break;
            case cc.macro.KEY.w:
                this._direction = CTRL_DIRECT.TOP;
            break;
            case cc.macro.KEY.s:
                this._direction = CTRL_DIRECT.BOTTOM;
            break;
            default:
                return;
        }
        MsgCenter.emitGameEvent(GameEvent.EVENT_KEY_DOWN, this._direction);
    }
    
    onKeyUp( evt : cc.Event.EventKeyboard){
        switch(evt.keyCode){
            case cc.macro.KEY.a:
            case cc.macro.KEY.d:
            case cc.macro.KEY.w:
            case cc.macro.KEY.s:
            break;
            default:
                return;
        }
        MsgCenter.emitGameEvent(GameEvent.EVENT_KEY_UP, this._direction);
    }
}   