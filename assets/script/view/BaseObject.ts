import { OBJ_TYPE } from "../config/GameEnum";
import { BaseUI } from "./base/BaseUI";

export default class BaseObject extends BaseUI {
    private _id = -1;
    private _name:String = "";
    private _type = OBJ_TYPE.NULL;

    public set Name(_name){
        this._name = _name;
    }

    public get Name(){
        return this._name;
    }

    public set Id(_id){
        this._id = _id;
    }

    public get Id(){
        return this._id;
    }

    public set Type(_type){
        this._type = _type;
    }

    public get Type(){
        return this._type;
    }
}   