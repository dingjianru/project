import { ViewManager } from "../util/ViewManager";
import { BaseScene } from "./base/BaseScene";
import MapNode from "./MapNode";
const {ccclass, property} = cc._decorator;

@ccclass
export class GameScene extends BaseScene {
    start(){
        super.start();
        ViewManager.load(MapNode);
    }
}